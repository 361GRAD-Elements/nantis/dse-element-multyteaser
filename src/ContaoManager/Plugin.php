<?php

namespace Dse\ElementsBundle\ElementMultyteaser\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementMultyteaser;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementMultyteaser\DseElementMultyteaser::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
