<?php

/**
 * 361GRAD Element Multyteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_multyteaser'] =
    '{type_legend},type;' .
    '{bg_legend},dse_bgcolor;' .
    '{fwe_legend},dse_isFullwidth,fwe_headline,fwe_text,fwe_link,fwe_linktext,fwe_target;' .
    '{2cols_legend},dse_is2cols,dse_isMirror,col_1_headline,col_1_text,col_1_link,col_1_linktext,col_1_target,col_1_image,col_1_imageSize,col_1_imageAlt,col_2_headline,col_2_text,col_2_link,col_2_linktext,col_2_target,col_2_image,col_2_imageSize,col_2_imageAlt;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['floating']['options']      = ['left', 'right'];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isFullwidth'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isFullwidth'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fwe_headline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['fwe_headline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fwe_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['fwe_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fwe_link'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['fwe_link'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fwe_target'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['fwe_target'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fwe_linktext'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['fwe_linktext'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fwe_image'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['fwe_image'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['fwe_imageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fwe_imageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['fwe_imageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_bgcolor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        'none',
        'bg-blue',
        'bg-lightgrey',
        'bg-white'
    ],
    'reference' => [
        'none'              => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#FFF',
            '#000',
            'Keine'
        ),
        'bg-blue'          => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#4a91d1',
            '#FFF',
            'Blue'
        ),
        'bg-lightgrey'      => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#d9d9d9',
            '#000',
            'LightGrey'
        ),
        'bg-white'          => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#FFF',
            '#000',
            'White'
        )
    ],
    'eval' => [
        'tl_class' => 'clr'
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_is2cols'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_is2cols'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isMirror'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isMirror'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_1_headline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_1_headline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_1_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_1_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_1_link'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_1_link'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_1_target'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_1_target'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_1_linktext'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_1_linktext'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_1_image'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_1_image'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['col_1_imageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_1_imageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_1_imageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w100'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_2_headline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_2_headline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_2_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_2_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_2_link'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_2_link'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_2_target'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_2_target'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_2_linktext'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_2_linktext'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_2_image'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_2_image'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['col_2_imageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['col_2_imageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['col_2_imageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w100'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
