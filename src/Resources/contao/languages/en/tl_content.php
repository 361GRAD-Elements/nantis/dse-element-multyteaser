<?php

/**
 * 361GRAD Element Multyteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_multyteaser'] = ['Multi-Teaser', 'Multi-Teaser with text and images.'];

$GLOBALS['TL_LANG']['tl_content']['bg_legend']   = 'Background Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']   = ['Background color', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_isFullwidth']   = ['Fullwidth Element', 'Check this if you want to use fullwidth element'];
$GLOBALS['TL_LANG']['tl_content']['fwe_legend']   = 'Fullwidth Element';
$GLOBALS['TL_LANG']['tl_content']['fwe_headline']   = ['Headline', ''];
$GLOBALS['TL_LANG']['tl_content']['fwe_text']   = ['Text', ''];
$GLOBALS['TL_LANG']['tl_content']['fwe_link']   = ['Link', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['fwe_target']   = ['Link Target', 'Open the link in a new browser window.'];
$GLOBALS['TL_LANG']['tl_content']['fwe_linktext']   = ['Link Text', 'The link text will be displayed instead of the target URL.'];
$GLOBALS['TL_LANG']['tl_content']['fwe_image']   = ['Image', 'Please select a file or folder from the files directory.'];
$GLOBALS['TL_LANG']['tl_content']['fwe_imageSize']   = ['Image Size', 'Here you can set the image dimensions and the resize mode.'];
$GLOBALS['TL_LANG']['tl_content']['fwe_imageAlt']   = ['Image Alt', 'You can enter an alternate text for the image (alt attribute) here.'];

$GLOBALS['TL_LANG']['tl_content']['dse_is2cols']   = ['Enable Element with Two columns', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_isMirror']   = ['Mirror Columns between each other', ''];

$GLOBALS['TL_LANG']['tl_content']['2cols_legend']   = 'Element with Two columns';
$GLOBALS['TL_LANG']['tl_content']['col_1_headline']   = ['Headline of the Left column', 'Here you can headline that will be using for left column only'];
$GLOBALS['TL_LANG']['tl_content']['col_1_text']   = ['Text of the Left column', ''];
$GLOBALS['TL_LANG']['tl_content']['col_1_link']   = ['Link of the Left column', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['col_1_target']   = ['Link Target', 'Open the link in a new browser window.'];
$GLOBALS['TL_LANG']['tl_content']['col_1_linktext']   = ['Link Text of the Left column', 'The link text will be displayed instead of the target URL.'];
$GLOBALS['TL_LANG']['tl_content']['col_1_image']   = ['Image of the Left column', 'Please select a file or folder from the files directory.'];
$GLOBALS['TL_LANG']['tl_content']['col_1_imageSize']   = ['Image Size', 'Here you can set the image dimensions and the resize mode.'];
$GLOBALS['TL_LANG']['tl_content']['col_1_imageAlt']   = ['Image Alt of the Left column', 'You can enter an alternate text for the image (alt attribute) here.'];

$GLOBALS['TL_LANG']['tl_content']['col_2_headline']   = ['Headline of the Right column', 'Here you can headline that will be using for right column only'];
$GLOBALS['TL_LANG']['tl_content']['col_2_text']   = ['Text of the Right column', ''];
$GLOBALS['TL_LANG']['tl_content']['col_2_link']   = ['Link of the Right column', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['col_2_target']   = ['Link Target', 'Open the link in a new browser window.'];
$GLOBALS['TL_LANG']['tl_content']['col_2_linktext']   = ['Link Text of the Right column', 'The link text will be displayed instead of the target URL.'];
$GLOBALS['TL_LANG']['tl_content']['col_2_image']   = ['Image of the Right column', 'Please select a file or folder from the files directory.'];
$GLOBALS['TL_LANG']['tl_content']['col_2_imageSize']   = ['Image Size', 'Here you can set the image dimensions and the resize mode.'];
$GLOBALS['TL_LANG']['tl_content']['col_2_imageAlt']   = ['Image Alt of the Right column', 'You can enter an alternate text for the image (alt attribute) here.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];