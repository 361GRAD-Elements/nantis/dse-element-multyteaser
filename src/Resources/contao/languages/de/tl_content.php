<?php

/**
 * 361GRAD Element Multyteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_multyteaser'] = ['Multi-Teaser', 'Multi-Teaser mit Text und Bildern.'];

$GLOBALS['TL_LANG']['tl_content']['bg_legend']   = 'Hintergrundeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']   = ['Hintergrundfarbe', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_isFullwidth']   = ['Volles Breitenelement', 'Überprüfen Sie dies, wenn Sie das Element voller Breite verwenden möchten'];
$GLOBALS['TL_LANG']['tl_content']['fwe_legend']   = 'Volles Breitenelement';
$GLOBALS['TL_LANG']['tl_content']['fwe_headline']   = ['Überschrift', ''];
$GLOBALS['TL_LANG']['tl_content']['fwe_text']   = ['Text', ''];
$GLOBALS['TL_LANG']['tl_content']['fwe_link']   = ['Link', 'Bitte geben Sie eine Webadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['fwe_target']   = ['Linkziel', 'Öffnet den Link in einem neuen Browserfenster.'];
$GLOBALS['TL_LANG']['tl_content']['fwe_linktext']   = ['Link Text', 'Der Linktext wird anstelle der Ziel-URL angezeigt.'];
$GLOBALS['TL_LANG']['tl_content']['fwe_image']   = ['Bild', 'Bitte wählen Sie eine Datei oder einen Ordner aus dem Dateiverzeichnis aus.'];
$GLOBALS['TL_LANG']['tl_content']['fwe_imageSize']   = ['Bildgröße', 'Hier können Sie die Bilddimensionen und den Größenänderungsmodus einstellen.'];
$GLOBALS['TL_LANG']['tl_content']['fwe_imageAlt']   = ['Bild Alt', 'Hier können Sie einen alternativen Text für das Bild eingeben (altattribut).'];

$GLOBALS['TL_LANG']['tl_content']['dse_is2cols']   = ['Enable Element mit zwei Spalten', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_isMirror']   = ['Spiegel Spalten zwischen einander', ''];

$GLOBALS['TL_LANG']['tl_content']['2cols_legend']   = 'Element mit zwei Spalten';
$GLOBALS['TL_LANG']['tl_content']['col_1_headline']   = ['Überschrift der linken Spalte', 'Hier kannst du Schlagzeile, die nur für die linke Spalte verwendet wird'];
$GLOBALS['TL_LANG']['tl_content']['col_1_text']   = ['Text der linken Spalte', ''];
$GLOBALS['TL_LANG']['tl_content']['col_1_link']   = ['Link der linken Spalte', 'Bitte geben Sie eine Webadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['col_1_target']   = ['Linkziel', 'Öffnet den Link in einem neuen Browserfenster.'];
$GLOBALS['TL_LANG']['tl_content']['col_1_linktext']   = ['Link Text der linken Spalte', 'Der Linktext wird anstelle der Ziel-URL angezeigt.'];
$GLOBALS['TL_LANG']['tl_content']['col_1_image']   = ['Bild der linken Spalte', 'Bitte wählen Sie eine Datei oder einen Ordner aus dem Dateiverzeichnis aus.'];
$GLOBALS['TL_LANG']['tl_content']['col_1_imageSize']   = ['Bildgröße', 'Hier können Sie die Bilddimensionen und den Größenänderungsmodus einstellen.'];
$GLOBALS['TL_LANG']['tl_content']['col_1_imageAlt']   = ['Bild Alt der linken Spalte', 'Hier können Sie einen alternativen Text für das Bild eingeben (altattribut).'];

$GLOBALS['TL_LANG']['tl_content']['col_2_headline']   = ['Überschrift der rechten Spalte', 'Hier kannst du Schlagzeile, die nur für die rechte Spalte verwendet wird'];
$GLOBALS['TL_LANG']['tl_content']['col_2_text']   = ['Text der rechten Spalte', ''];
$GLOBALS['TL_LANG']['tl_content']['col_2_link']   = ['Link der rechten Spalte', 'Bitte geben Sie eine Webadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['col_2_target']   = ['Linkziel', 'Öffnet den Link in einem neuen Browserfenster.'];
$GLOBALS['TL_LANG']['tl_content']['col_2_linktext']   = ['Link Text der rechten Spalte', 'Der Linktext wird anstelle der Ziel-URL angezeigt.'];
$GLOBALS['TL_LANG']['tl_content']['col_2_image']   = ['Bild der rechten Spalte', 'Bitte wählen Sie eine Datei oder einen Ordner aus dem Dateiverzeichnis aus.'];
$GLOBALS['TL_LANG']['tl_content']['col_2_imageSize']   = ['Bildgröße', 'Hier können Sie die Bilddimensionen und den Größenänderungsmodus einstellen.'];
$GLOBALS['TL_LANG']['tl_content']['col_2_imageAlt']   = ['Bild Alt der rechten Spalte', 'Hier können Sie einen alternativen Text für das Bild eingeben (altattribut).'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];